
package edu.uchicago.gerber.myfetchjson.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Thumbnail  implements Serializable {

    @SerializedName("default")
    @Expose
    public String _default;
    @SerializedName("hqDefault")
    @Expose
    public String hqDefault;

}
