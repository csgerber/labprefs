
package edu.uchicago.gerber.myfetchjson.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AccessControl implements Serializable {

    @SerializedName("syndicate")
    @Expose
    public String syndicate;
    @SerializedName("commentVote")
    @Expose
    public String commentVote;
    @SerializedName("rate")
    @Expose
    public String rate;
    @SerializedName("list")
    @Expose
    public String list;
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("embed")
    @Expose
    public String embed;
    @SerializedName("videoRespond")
    @Expose
    public String videoRespond;

}
