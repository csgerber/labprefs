package edu.uchicago.gerber.myfetchjson;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.Serializable;

import edu.uchicago.gerber.myfetchjson.jigs.Response;

public class ResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Bundle bundle = getIntent().getExtras();

        Serializable serializable = bundle.getSerializable(MainActivity.RESPONSE);
        Response response = (Response) serializable;
        Toast.makeText(this, response.toString(), Toast.LENGTH_SHORT).show();

    }
}
