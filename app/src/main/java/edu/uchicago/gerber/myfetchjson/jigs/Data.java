
package edu.uchicago.gerber.myfetchjson.jigs;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable {

    @SerializedName("updated")
    @Expose
    public String updated;
    @SerializedName("totalItems")
    @Expose
    public Integer totalItems;
    @SerializedName("startIndex")
    @Expose
    public Integer startIndex;
    @SerializedName("itemsPerPage")
    @Expose
    public Integer itemsPerPage;
    @SerializedName("items")
    @Expose
    public List<Item> items = null;

}
