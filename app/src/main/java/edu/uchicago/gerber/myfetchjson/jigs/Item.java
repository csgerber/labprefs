
package edu.uchicago.gerber.myfetchjson.jigs;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item  implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("uploaded")
    @Expose
    public String uploaded;
    @SerializedName("updated")
    @Expose
    public String updated;
    @SerializedName("uploader")
    @Expose
    public String uploader;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("tags")
    @Expose
    public List<String> tags = null;
    @SerializedName("thumbnail")
    @Expose
    public Thumbnail thumbnail;
    @SerializedName("player")
    @Expose
    public Player player;
    @SerializedName("content")
    @Expose
    public Content content;
    @SerializedName("duration")
    @Expose
    public Integer duration;
    @SerializedName("aspectRatio")
    @Expose
    public String aspectRatio;
    @SerializedName("rating")
    @Expose
    public Float rating;
    @SerializedName("ratingCount")
    @Expose
    public Integer ratingCount;
    @SerializedName("viewCount")
    @Expose
    public Integer viewCount;
    @SerializedName("favoriteCount")
    @Expose
    public Integer favoriteCount;
    @SerializedName("commentCount")
    @Expose
    public Integer commentCount;
    @SerializedName("status")
    @Expose
    public Status status;
    @SerializedName("accessControl")
    @Expose
    public AccessControl accessControl;

}
