package edu.uchicago.gerber.myfetchjson;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import edu.uchicago.gerber.myfetchjson.jigs.Response;

public class MainActivity extends AppCompatActivity {

    public static final String RESPONSE = "RESPONSE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setBackgroundColor();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new FetchYoutubeTask(MainActivity.this)
                        .execute("http://gerber.cs.uchicago.edu/android/youtube.json");

            }
        });
    }

    private void setBackgroundColor() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String strColor =
                prefs.getString(getResources().getString(R.string.background_color), "RED");
        View view = findViewById(R.id.root_layout);
        switch (strColor){

            case "RED":


                view.setBackgroundColor(Color.RED);

                break;

            case "GREEN":
                view.setBackgroundColor(Color.GREEN);
                break;

            case "BLUE":
                view.setBackgroundColor(Color.BLUE);

                break;
            default:
                view.setBackgroundColor(Color.RED);



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //pass in a string, no intermediate values, return Response
    class FetchYoutubeTask extends AsyncTask<String, Void, Response>{

        private Context context;

        public FetchYoutubeTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Response doInBackground(String... params) {
            return getResponse(params[0]);
        }

        @Override
        protected void onPostExecute(Response response) {
           /// super.onPostExecute(response);

            //pass data to another activity.
            Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(MainActivity.this, ResultsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(RESPONSE, response);
            intent.putExtras(bundle);

            context.startActivity(intent);


        }

        private Response getResponse(String strUrl) {

            StringBuilder stringBuilder = null;
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) new java.net.URL(strUrl).openConnection();
                int responseCode = connection.getResponseCode();

                if (responseCode < 300){

                    stringBuilder = new StringBuilder();
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                            connection.getInputStream()
                    )))
                    {
                        String line;
                        while ((line = reader.readLine()) != null){
                            stringBuilder.append(line);
                        }
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            Gson gson = new Gson();
            Response response = gson.fromJson(stringBuilder.toString(), Response.class);
            return  response;

        }
    }//end Async






}
