
package edu.uchicago.gerber.myfetchjson.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Player  implements Serializable {

    @SerializedName("default")
    @Expose
    public String _default;

}
