
package edu.uchicago.gerber.myfetchjson.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Response implements Serializable{

    @SerializedName("apiVersion")
    @Expose
    public String apiVersion;
    @SerializedName("data")
    @Expose
    public Data data;

    @Override
    public String toString() {
        return "Response{" +
                "apiVersion='" + apiVersion + '\'' +
                ", data=" + data +
                '}';
    }
}
