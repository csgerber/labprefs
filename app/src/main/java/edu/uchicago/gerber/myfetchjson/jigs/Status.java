
package edu.uchicago.gerber.myfetchjson.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Status  implements Serializable {

    @SerializedName("value")
    @Expose
    public String value;
    @SerializedName("reason")
    @Expose
    public String reason;

}
