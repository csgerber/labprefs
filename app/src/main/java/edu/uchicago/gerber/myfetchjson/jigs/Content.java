
package edu.uchicago.gerber.myfetchjson.jigs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Content  implements Serializable {

    @SerializedName("1")
    @Expose
    public String _1;
    @SerializedName("5")
    @Expose
    public String _5;
    @SerializedName("6")
    @Expose
    public String _6;

}
